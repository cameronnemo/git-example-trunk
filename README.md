## Initialization

```
➜ git config --global init.defaultBranch trunk
➜ git init
Inicializado repositorio Git vacío en /home/cameronnemo/src/cameronnemo/trunk/.git/
➜ git:(trunk) touch init
➜ git:(trunk) ✗ git add -A
➜ git:(trunk) ✗ git commit -m 'init'
[trunk (commit-raíz) 12518a0] init
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 init
➜ git:(trunk) git tag 0.0.1
```

## Features, fixes, and breaking changes

```
➜ git:(trunk) touch feature
➜ git:(trunk) ✗ git add -A
➜ git:(trunk) ✗ git commit -m 'feature'
[trunk 6a7385d] feature
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 feature
➜ git:(trunk) touch fix
➜ git:(trunk) ✗ git add -A
➜ git:(trunk) ✗ git commit -m 'fix'
[trunk d2e1207] fix
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 fix
➜ git:(trunk) git tag 0.0.2
➜ git:(trunk) touch break
➜ git:(trunk) ✗ git add -A
➜ git:(trunk) ✗ git commit -m 'break'
[trunk 780c1a9] break
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 break
➜ git:(trunk) git tag 0.1.0
```

## Security fix that is backported

```
➜ git:(trunk) touch 'security fix'
➜ git:(trunk) ✗ git add -A
➜ git:(trunk) ✗ git commit -m 'security fix'
[trunk 144db51] security fix
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 security fix
➜ git:(trunk) git tag 0.1.1
➜ git:(trunk) git log --oneline
144db51 (HEAD -> trunk, tag: 0.1.1) security fix
780c1a9 (tag: 0.1.0) break
d2e1207 (tag: 0.0.2) fix
6a7385d feature
12518a0 (tag: 0.0.1) init
➜ git:(trunk) git checkout -b hotfix/0.0 0.0.2
Cambiado a nueva rama 'hotfix/0.0'
➜ git:(hotfix/0.0) git cherry-pick 144db51
[hotfix/0.0 91dbd64] security fix
 Date: Sat Feb 6 12:57:17 2021 -0800
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 security fix
➜ git:(hotfix/0.0) git tag 0.0.3
➜ git:(hotfix/0.0) git log --oneline
91dbd64 (HEAD -> hotfix/0.0, tag: 0.0.3) security fix
d2e1207 (tag: 0.0.2) fix
6a7385d feature
12518a0 (tag: 0.0.1) init
```

## Stabilization of repository and pre-release

```
➜ git:(trunk) touch stabilization
➜ git:(trunk) ✗ git add -A
➜ git:(trunk) ✗ git commit -m 'stabilization'
[trunk d26edd0] stabilization
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 stabilization
➜ git:(trunk) git tag 1.0.0-rc1
```

## Feature branch and another pre-release

```
➜ git:(trunk) git checkout -b feature/for-1.0
Cambiado a nueva rama 'feature/for-1.0'
➜ git:(feature/for-1.0) touch feature-for-1.0
➜ git:(feature/for-1.0) ✗ git add -A
➜ git:(feature/for-1.0) ✗ git commit --author 'Alternate Cameron <alt@cam>' -m 'feature-for-1.0'
[feature/for-1.0 dea6fe6] feature-for-1.0
 Author: Alternate Cameron <alt@cam>
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 feature-for-1.0
➜ git:(feature/for-1.0) git checkout trunk
Cambiado a rama 'trunk'
➜ git:(trunk) git merge --no-ff feature/for-1.0
Merge made by the 'recursive' strategy.
 feature-for-1.0 | 0
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 feature-for-1.0
➜ git:(trunk) git tag 1.0.0-rc2
```

## Another security fix to backport

```
➜ git:(trunk) git checkout -b security/for-1.0
Cambiado a nueva rama 'security/for-1.0'
➜ git:(security/for-1.0) touch 'security for 1.0'
➜ git:(security/for-1.0) ✗ git add -A
➜ git:(security/for-1.0) ✗ git commit --author 'Alternate Cameron <alt@cam>' -m 'security for 1.0'
[security/for-1.0 e74da75] security for 1.0
 Author: Alternate Cameron <alt@cam>
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 security for 1.0
➜ git:(security/for-1.0) git checkout trunk
Cambiado a rama 'trunk'
➜ git:(trunk) git merge --no-ff security/for-1.0
Merge made by the 'recursive' strategy.
 security for 1.0 | 0
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 security for 1.0
➜ git:(trunk) git tag 1.0.0-rc3
➜ git:(trunk) git log --oneline
485cb1a (HEAD -> trunk, tag: 1.0.0-rc3) Merge branch 'security/for-1.0' into trunk
e74da75 (security/for-1.0) security for 1.0
a69e9c4 (tag: 1.0.0-rc2) Merge branch 'feature/for-1.0' into trunk
dea6fe6 (feature/for-1.0) feature-for-1.0
d26edd0 (tag: 1.0.0-rc1) stabilization
144db51 (tag: 0.1.1) security fix
780c1a9 (tag: 0.1.0) break
d2e1207 (tag: 0.0.2) fix
6a7385d feature
12518a0 (tag: 0.0.1) init
➜ git:(trunk) git checkout -b hotfix/0.1 0.1.1
Cambiado a nueva rama 'hotfix/0.1'
➜ git:(hotfix/0.1) git cherry-pick e74da75
[hotfix/0.1 b7b4b41] security for 1.0
 Author: Alternate Cameron <alt@cam>
 Date: Sat Feb 6 13:22:27 2021 -0800
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 security for 1.0
➜ git:(hotfix/0.1) git tag 0.1.2
➜ git:(hotfix/0.1) git log --oneline
b7b4b41 (HEAD -> hotfix/0.1, tag: 0.1.2) security for 1.0
144db51 (tag: 0.1.1) security fix
780c1a9 (tag: 0.1.0) break
d2e1207 (tag: 0.0.2) fix
6a7385d feature
12518a0 (tag: 0.0.1) init
```

## Stable release

```
➜ git:(trunk) touch 'finalize 1.0'
➜ git:(trunk) ✗ git add -A
➜ git:(trunk) ✗ git commit -m 'finalize 1.0'
[trunk 3ddef23] finalize 1.0
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 finalize 1.0
➜ git:(trunk) git tag 1.0.0
➜ git:(trunk) git log --oneline
3ddef23 (HEAD -> trunk, tag: 1.0.0) finalize 1.0
485cb1a (tag: 1.0.0-rc3) Merge branch 'security/for-1.0' into trunk
e74da75 (security/for-1.0) security for 1.0
a69e9c4 (tag: 1.0.0-rc2) Merge branch 'feature/for-1.0' into trunk
dea6fe6 (feature/for-1.0) feature-for-1.0
d26edd0 (tag: 1.0.0-rc1) stabilization
144db51 (tag: 0.1.1) security fix
780c1a9 (tag: 0.1.0) break
d2e1207 (tag: 0.0.2) fix
6a7385d feature
12518a0 (tag: 0.0.1) init
```
